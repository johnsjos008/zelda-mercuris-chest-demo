<div align="center" style="padding: 2em;">
  <img src="data/logos/thumbnail.png"/>
</div>

# The Legend of Zelda: Mercuris' Chest

## About

This repository contains the data files of the Solarus quest *The Legend of Zelda: Mercuris' Chest*. 

This quest is a free, open-source game that works with Solarus, a free and open-source Action-RPG 2D game engine. See the [Solarus website](https://www.solarus-games.org) to get more information and how to use Solarus.

- **Release Date:** TBA
- **Players:** 1
- **Length:** TBA
- **License:** Copyright, CC BY-SA 4.0, GPL v3
- **Languages:** French, English

## License

This repository contains files under multiple licenses:

- Source code is licensed under the terms of the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.html) (GPL v3).
- Resources are licensed under the terms of the [Creative Commons Attribution-ShareAlike 4.0](http://creativecommons.org/licenses/by-sa/4.0) (CC BY-SA 4.0).
- Graphics, sounds and names that belong to Nintendo are copyrighted.

For more detailed information about licensing, see [license.txt](/license.txt).