local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

require("enemies/lib/util").enrich(enemy)

-- Constants
local circles = {
  [1] = math.pi,
  [2] = 3 * math.pi / 2,
  [3] = 3 * math.pi / 4,
  [4] = 5 * math.pi / 4,
}

-- Properties
local triggering_distance = 64
local state = "waiting"

local wait_timer
local sprite

function enemy:on_created()

  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(1)
  enemy:set_damage(1)
  enemy:set_obstacle_behavior("flying")
  enemy:set_layer_independent_collisions(true)
  enemy:set_size(12, 12)
  enemy:set_origin(6, 10)
  enemy:set_attacking_collision_mode("overlapping")
end

function enemy:on_restarted()

  enemy:restart(state)
end

function enemy:restart(action)

  state = action
  if state == "flying" then
    enemy:fly()
  elseif state == "recover" then
    enemy:recover()
  else
    enemy:stop()
  end
end

function enemy:fly()

  sprite:set_animation("flying")
  sol.audio.play_sound("alttp/keese")

  -- Random starting angle
  local angle = 0
  local angle = math.random(0, 2 * math.pi)
  local angle_limit = circles[math.random(1, 4)]

  -- Movement
  local movement = sol.movement.create("straight")
  movement:set_ignore_obstacles(true)
  movement:set_angle(angle)
  movement:set_max_distance(100)
  movement:set_speed(64)
  movement:start(enemy)

  -- Change the angle over time to make a circle movement
  local angle_timer = 50
  local angle_progress = 0

  sol.timer.start(enemy, 50, function()
    
    angle_progress = angle_progress + 0.1
    angle = angle + 0.1
    movement:set_angle(angle)
    
    if angle_progress >= angle_limit then
      -- Stop this timer
      angle_timer = 0

      -- Stop the enemy movement
      movement:stop()
      enemy:restart("recover")
    end

    angle_timer = angle_timer - 1
    return angle_timer > 0
  end)
end

-- Wait a bit after flying to not fly constantly
function enemy:recover()

  sprite:set_animation("stopped")
  sol.timer.start(enemy, 500, function()
    enemy:restart("stop")
  end)
end

function enemy:stop()

  sprite:set_animation("stopped")

  if wait_timer then
    wait_timer:stop()
  end

  wait_timer = sol.timer.start(enemy, 50, function()
    -- Check 50 times every 50ms if the hero is near
    if not enemy:is_near(hero, triggering_distance) then
      return 50
    end
    enemy:restart("flying")
    wait_timer = nil
  end)
end
