local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()

local sprite
local shield

local movement

local state = "waiting"

function enemy:on_created()

  -- Sprites
  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  shield = enemy:create_sprite("enemies/" .. enemy:get_breed() .. "_shield")
  shield:set_animation("stopped")

  -- Boss properties
  enemy:set_life(6)
  enemy:set_damage(2)
  enemy:set_pushed_back_when_hurt(false)
  enemy:set_hurt_style("boss")
  enemy:set_attacking_collision_mode("overlapping")
  enemy:set_size(20, 20)
  enemy:set_origin(10, 14)

  -- Disable all woosh
  for woosh in map:get_entities("boss_woosh") do
    woosh:set_enabled(false)
  end
end

function enemy:on_restarted()

  -- Boss is resistant to physical attacks but fire
  enemy:set_attack_consequence("thrown_item", "protected")
  enemy:set_attack_consequence("sword", "protected")
  enemy:set_attack_consequence("explosion", "protected")

  sprite:set_animation("stopped")

  if state == "spell" then
    enemy:do_spell_attack()
  elseif state == "guard" then
    enemy:guard()
  elseif state == "waiting" then
    enemy:set_shield_enabled(false)
  end
end

-- Called by map to actually start the boss fight
function enemy:start()

  enemy:set_shield_enabled(true)
  state = "spell"
  enemy:do_spell_attack()
end

function enemy:do_run()

  enemy:set_shield_enabled(false)
  sprite:set_animation("walking")

  local angles = {
    [1] = 0,
    [2] = math.pi,
    [3] = math.pi / 2,
    [4] = 3 * math.pi / 2,
  }
  local directions = {
    [1] = 0,
    [2] = 2,
    [3] = 1,
    [4] = 3,
  }
  
  local random_direction = math.random(2)
  sprite:set_direction(directions[random_direction])

  -- Do run movement
  movement = sol.movement.create("straight")
  movement:set_speed(128)
  movement:set_angle(angles[random_direction])
  movement:start(enemy)
  movement:set_max_distance(300)
  movement.on_obstacle_reached = function()
    local random_angle = math.random(4)
    sprite:set_direction(directions[random_angle])
    movement:set_angle(angles[random_angle])
  end

  -- Run for 5 sec
  sol.timer.start(enemy, 5000, function()
    movement:stop()
    state = "spell"
    enemy:do_spell_attack()
  end)
end

function enemy:guard()

  enemy:set_shield_enabled(true)
  sol.timer.start(enemy, 1000, function()
    state = "spell"
    enemy:on_restarted()
  end)
end

function enemy:do_spell_attack()

  local colors = {
    [1] = "red",
    [2] = "green",
    [3] = "blue",
  }

  sprite:set_animation("focus")
  sol.audio.play_sound("boss_charge")
  sol.timer.start(enemy, 2000, function()

    local count = 0

    sprite:set_animation("spell")
    sol.audio.play_sound("cane")

    sol.timer.start(enemy, 500, function()
      -- for each keese invoked, random color
      local color_id = math.random(3)

      -- according boss life, more and more keeses
      if enemy:get_life() >= 6 then
        enemy:do_invoke_one_keese(colors[color_id])
      elseif enemy:get_life() >= 5 then
        enemy:do_invoke_one_keese(colors[color_id])
        count = count + 1
        return count < 3
      elseif enemy:get_life() >= 3 then
        enemy:do_invoke_one_keese(colors[color_id])
        count = count + 1
        return count < 6
      else
        enemy:do_invoke_one_keese(colors[color_id])
        count = count + 1
        return count < 12
      end
    end)

    sol.timer.start(enemy, 2000, function()
      state = "run"
      enemy:do_run()
    end)
  end)
end

-- Called on hurt
function enemy:on_hurt()
  state = "guard"
end

-- Set shield enabled or not (visually and link can hurt Shamablin)
function enemy:set_shield_enabled(enabled)

  if enabled == true then
    shield:set_opacity(128)
    enemy:set_attack_consequence("fire", "protected")
  else
    shield:set_opacity(0)
    enemy:set_attack_consequence("fire", 1)
  end
end

function enemy:do_invoke_one_keese(color)

  local woosh = map:get_entity("boss_woosh_" .. color)
  woosh:set_enabled(true)
  sol.audio.play_sound("splash")
  sol.audio.play_sound("sword4")
  woosh:get_sprite():set_animation("woosh")

  local x, y, layer = woosh:get_position()

  local enemy = map:create_enemy({
    breed = "bosses/shamablin/shamablin_keese_" .. color,
    layer = layer,
    x = x,
    y = y,
    direction = 0,
  })
end

function enemy:on_dying()

  hero:freeze()
  game:start_dialog("dungeon_2.boss.dying")
end

local function demo_end()
  
  sol.audio.play_sound("world_warp")
  sol.audio.stop_music()
  sol.timer.start(map, 4000, function()
    hero:teleport("cutscenes/demo_ending")
  end)

  local shader = sol.shader.create("distorsion")
  local magnitude = 0.0001
  local magn_incr = 0.0005
  map:get_camera():get_surface():set_shader(shader)
  sol.timer.start(map, 16.66, function()
    shader:set_uniform("magnitude", magnitude)
    magnitude = magnitude + magn_incr
    magn_incr = magn_incr + 0.00001

    return magnitude < 0.5
  end)
end

function enemy:on_dead()

  sol.timer.start(map, 1000, function()
    demo_end()
  end)
end

