local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite
local movement

function enemy:on_created()

  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(1)
  enemy:set_damage(1)
  enemy:set_size(8, 8)
  enemy:set_origin(4, 5)
  enemy:set_invincible()
  enemy:set_minimum_shield_needed(1)
  enemy:set_can_hurt_hero_running(true)
end

function enemy:on_restarted()

  local angles = {
    0,
    math.pi / 2,
    math.pi,
    3 * math.pi / 2,
  }

  movement = sol.movement.create("straight")
  movement:set_angle(angles[sprite:get_direction() + 1])
  movement:set_speed(148)
  movement:set_smooth(false)
  movement:start(enemy)
  movement.on_obstacle_reached = function()
    movement:stop()
    enemy:remove()
  end
end
