local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local sprite

function enemy:on_created()

  sprite = enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(1)
  enemy:set_damage(2)
  enemy:set_invincible()
  enemy:set_size(64, 24)
  enemy:set_origin(32, 12)
end

function enemy:on_restarted()

  sprite:set_animation("stopped")
end
