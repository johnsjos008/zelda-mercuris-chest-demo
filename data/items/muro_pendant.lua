local item = ...
local game = item:get_game()

function item:on_created()
  
  self:set_savegame_variable("item_status_muro_pendant")
  self:set_assignable(false)
end
