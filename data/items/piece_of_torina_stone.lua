local item = ...

function item:on_obtaining(variant, savegame_variable)

  local piece_of_torina_stone_counter = self:get_game():get_item("piece_of_torina_stone_counter")
  if piece_of_torina_stone_counter:get_variant() == 0 then
    piece_of_torina_stone_counter:set_variant(1)
  end
  piece_of_torina_stone_counter:add_amount(1)
end