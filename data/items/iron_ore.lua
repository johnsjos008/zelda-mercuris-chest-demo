local item = ...
local game = item:get_game()

function item:on_created()
  
  self:set_savegame_variable("item_status_iron_ore")
  self:set_assignable(false)
end
