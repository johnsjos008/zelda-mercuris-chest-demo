local pillar = ...
local map = pillar:get_map()
local game = map:get_game()

-- Properties
local pos_x, pos_y, layer = pillar:get_position()
local init_floor = "1f"
local init_savegame_variable

-- Animations
local animation_sprites = {
  clockwise = nil,
  counter_clockwise = nil,
}

-- Dynamic tiles
local base_tile
local corner_pillars = {}

-- Dynamic tiles config
local base_offsets = { 13, 19 }
local dt_config = {
  corners = {
    -- Top left (bottom)
    [1] = {
      x = base_offsets[1],
      y = base_offsets[2],
      layer = layer
    },
    -- Top right (bottom)
    [2] = {
      x = base_offsets[1] + 48,
      y = base_offsets[2],
      layer = layer
    },
    -- Bottom left (bottom)
    [3] = {
      x = base_offsets[1],
      y = base_offsets[2] + 48,
      layer = layer
    },
    -- Bottom right (bottom)
    [4] = {
      x = base_offsets[1] + 48,
      y = base_offsets[2] + 48,
      layer = layer
    },
  },
  blocks = {
    top = {
      [1] = {
        x = base_offsets[1] + 16,
        y = base_offsets[2],
        layer = layer,
      },
      [2] = {
        x = base_offsets[1] + 32,
        y = base_offsets[2],
        layer = layer,
      },
    },
    left = {
      [1] = {
        x = base_offsets[1],
        y = base_offsets[2] + 16,
        layer = layer,
      },
      [2] = {
        x = base_offsets[1],
        y = base_offsets[2] + 32,
        layer = layer,
      },
    },
    right = {
      [1] = {
        x = base_offsets[1] + 48,
        y = base_offsets[2] + 16,
        layer = layer,
      },
      [2] = {
        x = base_offsets[1] + 48,
        y = base_offsets[2] + 32,
        layer = layer,
      },
    },
    bottom = {
      [1] = {
        x = base_offsets[1] + 16,
        y = base_offsets[2] + 48,
        layer = layer,
      },
      [2] = {
        x = base_offsets[1] + 32,
        y = base_offsets[2] + 48,
        layer = layer,
      },
    }
  }
}

local function create_base()

  base_tile = map:create_dynamic_tile({
    layer = layer,
    x = pos_x + 13,
    y = pos_y + 19,
    width = 64,
    height = 64,
    pattern = "floor.10-1",
    enabled_at_start = true,
  })
end

local function create_corner_pillars()

  -- Create bottoms
  for i, corner in ipairs(dt_config.corners) do
    corner_pillars[#corner_pillars + 1] = map:create_dynamic_tile({
      layer = corner.layer,
      x = pos_x + corner.x,
      y = pos_y + corner.y,
      width = 16,
      height = 16,
      pattern = "pillar.bottom",
      enabled_at_start = true,
    })
    corner_pillars[#corner_pillars + 1] = map:create_dynamic_tile({
      layer = corner.layer + 1,
      x = pos_x + corner.x,
      y = pos_y + corner.y - 16,
      width = 16,
      height = 16,
      pattern = "pillar.top",
      enabled_at_start = true,
    })
  end
end

local function create_blocks(side)

  for i, block in ipairs(dt_config.blocks[side]) do
    map:create_dynamic_tile({
      name = "rp_block_" .. side .. "_bottom_" .. i,
      layer = block.layer,
      x = pos_x + block.x,
      y = pos_y + block.y,
      width = 16,
      height = 16,
      pattern = "pillar.bottom",
      enabled_at_start = false,
    })
    map:create_dynamic_tile({
      name = "rp_block_" .. side .. "_top_" .. i,
      layer = block.layer + 1,
      x = pos_x + block.x,
      y = pos_y + block.y - 16,
      width = 16,
      height = 16,
      pattern = "pillar.top",
      enabled_at_start = false,
    })
  end
end

local function create_blocking_pillars()

  if init_floor == "1f" then
    create_blocks("left")
    create_blocks("right")
  else
    create_blocks("top")
    create_blocks("bottom")
  end
end

local function set_block_enabled(side, enabled)

  for block in map:get_entities("rp_block_" .. side) do
    block:set_enabled(enabled)
  end
end

local function set_side_open(side)

  if init_floor == "1f" then
    -- 1F
    if side == "right" then
      -- right
      set_block_enabled("left", true) -- left is closed
      set_block_enabled("right", false) -- right is open
    else
      -- left
      set_block_enabled("left", false) -- left is open
      set_block_enabled("right", true) -- right is closed
    end
  else
    -- B2
    if side == "top" then
      -- top
      set_block_enabled("top", false) -- top is open
      set_block_enabled("bottom", true) -- bottom is closed
    else
      -- bottom
      set_block_enabled("top", true) -- top is closed
      set_block_enabled("bottom", false) -- bottom is open
    end
  end
end

-- Create animation sprite for this entity
local function create_animations()

  pillar:create_sprite("entities/dungeons/rotating_pillar", "rotating_pillar")
  pillar:get_sprite("rotating_pillar"):stop_animation()
end

-- Enable or disable all dynamic tiles (including blocks)
local function enable_dynamic_tiles(enabled)

  base_tile:set_enabled(enabled)
  for i, corner in ipairs(corner_pillars) do
    corner:set_enabled(enabled)
  end
  for block in map:get_entities("rp_block") do
    block:set_enabled(enabled)
  end
end

-- Show the rotating pillar animation from sprite
local function start_sprite_animation(animation_set, on_finished)
  local sprite = pillar:get_sprite("rotating_pillar")
  local animation = "turn_" .. init_floor .. "_" .. animation_set
  sprite:set_animation(animation)
  sprite.on_animation_finished = function()
    sol.audio.play_sound("oot/block_drop")
    on_finished()
  end
end

-- Init the rotating pillar entity, to be called in map on_started event
-- floor can be "1f" or "b2"
function pillar:init(config)

  init_floor = config.floor
  init_savegame_variable = config.savegame_variable

  create_base()
  create_corner_pillars()
  create_blocking_pillars(floor)
  create_animations()

  local saved_side = game:get_value(init_savegame_variable)
  if saved_side == nil then
    saved_side = config.default_side -- default value for this rotating platform
  end
  set_side_open(saved_side)
end

-- Rotate the pillar clockwise
function pillar:rotate_clockwise(on_finished)
  sol.audio.play_sound("alttp/quake")
  
  -- Hide all static tiles
  enable_dynamic_tiles(false)

  -- Actually turn the pillar
  start_sprite_animation("clockwise", function()
    
    -- Show static tiles
    enable_dynamic_tiles(true)

    if init_floor == "1f" then
      -- Open only left side for 1F
      set_side_open("left")
      -- Save the side status
      game:set_value(init_savegame_variable, "left")
    else
      -- Open only bottom side for B2
      set_side_open("bottom")
      -- Save the side status
      game:set_value(init_savegame_variable, "bottom")
    end

    on_finished()
  end)
end

-- Rotate the pillar counter clockwise
function pillar:rotate_counter_clockwise(on_finished)
  sol.audio.play_sound("alttp/quake")
  
  -- Hide all static tiles
  enable_dynamic_tiles(false)

  -- Actually turn the pillar
  start_sprite_animation("counter_clockwise", function()
    
    -- Show static tiles
    enable_dynamic_tiles(true)

    if init_floor == "1f" then
      -- Open only right side for 1F
      set_side_open("right")
      -- Save the side status
      game:set_value(init_savegame_variable, "right")
    else
      -- Open only top side for B2
      set_side_open("top")
      -- Save the side status
      game:set_value(init_savegame_variable, "top")
    end

    on_finished()
  end)
end