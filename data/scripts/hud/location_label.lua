
local language_manager = require("scripts/language_manager")

local location_label_builder = {}

function location_label_builder:new(game, config)

  local location_label = {}
  location_label.visible = true
  location_label.previous_location = nil

  function location_label:on_map_changed(map)
    
    local opacity = 0
    local world = map:get_world()
    local should_show = false

    location_label.frame = sol.surface.create("hud/location_label_frame.png")

    -- Load the label depending on the map
    if map ~= nil then
      local font_name, font_size = language_manager:get_label_font()
      if world == "outside" then
        -- Outside regions of the world map
        local text = sol.language.get_string("location_labels." .. map:get_id())
        location_label.label = sol.text_surface.create({
          text = text,
          font = font_name,
          font_size = font_size,
        })
        location_label.shadow = sol.text_surface.create({
          text = text,
          font = font_name,
          font_size = font_size,
          color = { 39, 39, 71 },
        })
        location_label.previous_location = map:get_id()
        should_show = true
      elseif world ~= nil then
        -- Dungeons
        if location_label.previous_location ~= world then
          -- Go in or out a dungeon
          local text = sol.language.get_string("location_labels." .. world)
          location_label.label = sol.text_surface.create({
            text = text,
            font = font_name,
            font_size = font_size,
          })
          location_label.shadow = sol.text_surface.create({
            text = text,
            font = font_name,
            font_size = font_size,
            color = { 39, 39, 71 },
          })
          location_label.previous_location = world
          should_show = true
        else
          -- Same dungeon but different floor
          location_label.label = nil
          location_label.shadow = nil
          location_label.frame = nil
        end
      else
        -- Other locations: no title label
        location_label.label = nil
        location_label.shadow = nil
        location_label.frame = nil
      end
    end

    -- Fade out the label using timers with map context
    local function fade_out()
      sol.timer.start(map, 16, function()
        opacity = opacity - 6
        if opacity < 0 then
          opacity = 0
        end
        location_label.frame:set_opacity(opacity)
        location_label.shadow:set_opacity(opacity)
        location_label.label:set_opacity(opacity)
        return opacity > 0
      end)
    end

    -- Fade in the label using timers with map context
    local function fade_in()
      sol.timer.start(map, 16, function()
        opacity = opacity + 6
        if opacity > 255 then
          opacity = 255
          sol.timer.start(map, 4000, fade_out)
        end
        location_label.frame:set_opacity(opacity)
        location_label.shadow:set_opacity(opacity)
        location_label.label:set_opacity(opacity)
        return opacity < 255
      end)
    end

    -- Draw the label after 1sec
    if should_show then
      location_label:set_visible(true)
      if location_label.label ~= nil then
        location_label.frame:set_opacity(0)
        location_label.shadow:set_opacity(0)
        location_label.label:set_opacity(0)
        sol.timer.start(map, 1000, function()
          fade_in()
        end)
      else
        print("warning: no location label found for current map/world")
      end
    else
      location_label:set_visible(false)
    end
  end

  function location_label:on_draw(dst_surface)
    if location_label.visible then
      if location_label.label ~= nil then
        location_label.frame:draw(dst_surface, config.x, config.y)
        location_label.shadow:draw(dst_surface, config.x + 17, config.y + 12)
        location_label.label:draw(dst_surface, config.x + 16, config.y + 11)
      end
    end
  end

  function location_label:set_visible(visible)
    location_label.visible = visible
  end

  return location_label

end

return location_label_builder