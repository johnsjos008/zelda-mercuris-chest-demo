local contributors_builder = {}
local language_manager = require("scripts/language_manager")

local names = {
  directors = {
    "Metallizer",
    "Christopho",
  },
  story = {
    "Metallizer",
    "Niko",
  },
  graphics = {
    "NewLink",
    "Olivier Clero",
    "std::gregwar",
  },
  level_design = {
    "Christopho",
    "NewLink",
    "Metallizer",
  },
  communication = {
    "Olivier Clero",
  },
  testing = {
    "Christopho",
  },
  thanks1 = {
    "Maxs",  -- Debug console, minimap system
    "Marine",  -- Intro mapping
    "Estelle",  -- Intro mapping
    "Vlag",  -- Touch control system
    "Valoo",  -- Deku Forest
    "Binbin",  -- Castle
  },
  thanks2 = {
    "BenObiWan",  -- Cane of Stone
    "Renkineko",  -- Bubble fix
    "Clem",  -- Graveyard
    "Sam101",  -- Rail temple
    "Mymy",  -- Rail temple
    "angenoir37",  -- Rail temple¨
  }
}

local function get_category_from_map_id(map_id)
  local categories = {
    ["out/c3"] = "directors",
    ["out/f3"] = "story",
    ["out/d1"] = "graphics",
    ["out/f7"] = "level_design",
    ["out/g7"] = "communication",
    ["out/g1"] = "testing",
    ["out/f4"] = "thanks1",
    ["out/a7"] = "thanks2",
  }
  return categories[map_id]
end

function contributors_builder:new(game, config)
  
  local contributors = {}
  contributors.visible = false
  
  local name_font = language_manager:get_credits_font()
  local job_font, job_font_size = language_manager:get_dialog_font()

  function contributors:on_map_changed(map)
    contributors.names = {}

    if game:get_value("demo_ending") then
      local name_category = get_category_from_map_id(map:get_id())

      local job_config = {
        horizontal_alignment = "right",
        font = job_font,
        font_size = job_font_size,
        text = sol.language.get_string("credits." .. name_category),
      }
      local job_stroke_config = {
        horizontal_alignment = "right",
        font = job_font,
        font_size = job_font_size,
        text = sol.language.get_string("credits." .. name_category),
        color = { 0, 0, 0 }
      }
      contributors.job_surface = sol.text_surface.create(job_config)
      contributors.job_surface_stroke_1 = sol.text_surface.create(job_stroke_config)
      contributors.job_surface_stroke_2 = sol.text_surface.create(job_stroke_config)
      contributors.job_surface_stroke_3 = sol.text_surface.create(job_stroke_config)
      contributors.job_surface_stroke_4 = sol.text_surface.create(job_stroke_config)

      if name_category ~= nil then
        for index, name in ipairs(names[name_category]) do
          contributors.names[index] = sol.text_surface.create({
            horizontal_alignment = "right",
            font = name_font,
            text = name,
          })
        end
      end
    end
  end

  function contributors:on_draw(surface)

    if contributors.visible then
      local offset_y = config.y + 48

      -- Display job
      -- TODO: need to use a util function to draw a text with a stroke
      contributors.job_surface_stroke_1:draw(surface, 304 + 1, offset_y)
      contributors.job_surface_stroke_2:draw(surface, 304 - 1, offset_y)
      contributors.job_surface_stroke_3:draw(surface, 304, offset_y + 1)
      contributors.job_surface_stroke_4:draw(surface, 304, offset_y - 1)
      contributors.job_surface:draw(surface, 304, offset_y)
      offset_y = offset_y + 16

      -- Display names
      for _, name_surface in ipairs(contributors.names) do
        name_surface:draw(surface, 304, offset_y)
        offset_y = offset_y + 16
      end
    end
  end

  function contributors:set_visible(visible)
    contributors.visible = visible
  end

  return contributors
end

return contributors_builder
