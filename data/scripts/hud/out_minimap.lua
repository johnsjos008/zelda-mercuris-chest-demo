
local minimap_builder = {}

function minimap_builder:new(game, config)

  -- Configurable constants
  local minimap_width = 64
  local minimap_height = 64
  local cursor_x_offset = 0
  local cursor_y_offset = 0

  -- Main object
  local minimap = {}

  -- Retrieve coordinates hud_from config
  if config ~= nil then
    minimap.dst_x, minimap.dst_y = config.x, config.y
  end

  minimap.visible = true

  -- Main surface
  minimap.surface = sol.surface.create(66, 66)

  -- Position cursor (current position)
  minimap.cursor = sol.sprite.create("hud/minimap_cursor")
  minimap.cursor:set_animation("current")

  -- From cursor (initial position)
  minimap.from = sol.sprite.create("hud/minimap_cursor")
  minimap.from:set_animation("initial")
  minimap.from_x = 0
  minimap.from_y = 0

  function minimap:on_map_changed(map)
    minimap:create(map)
  end

  function minimap:create(map)
    -- Retrieve minimap sprite for this map
    minimap.img = sol.surface.create("hud/out_minimaps/" .. map:get_id() .. ".png")

    if minimap.img ~= nil then
      minimap_width, minimap_height = minimap.img:get_size()
      minimap.img:set_opacity(192)

      -- Set the converted coords of initial position cursor (red)
      minimap.from_x, minimap.from_y = map:get_hero():get_position()
      minimap.from:set_direction(map:get_hero():get_direction())
    else
      if map:get_world() == "outside" then
        print("warning: unable to find minimap with id " .. map:get_id())
      end
    end
  end

  function minimap:on_draw(dst_surface)

    if minimap.visible and minimap.img ~= nil then
      local x, y = minimap.dst_x, minimap.dst_y
      local width, height = dst_surface:get_size()

      -- Handle negative coord values
      if x < 0 then
        x = width + x
      end
      if y < 0 then
        y = height + y
      end

      -- Fix position depending on the size of the map
      local offset_x = 64 - minimap_width
      local offset_y = 64 - minimap_height

      -- Draw the map and cursor
      minimap:draw_all()

      -- Draw the main surface
      minimap.surface:draw(dst_surface, x + offset_x, y + offset_y)
    end
  end

  -- Called on each draw
  function minimap:draw_all()

    -- Clear the main surface before
    minimap.surface:clear()

    -- Draw the minimap image on the main surface
    if minimap.img ~= nil then
      minimap.img:draw(minimap.surface)
    end
    
    local map = game:get_map()

    -- Draw the initial cursor (red)
    local conv_x, conv_y = minimap:convert_coords(minimap.from_x, minimap.from_y)
    minimap.from:draw(minimap.surface, conv_x, conv_y)

    -- Change current cursor direction
    minimap.cursor:set_direction(map:get_hero():get_direction())

    -- Draw the cursor (yellow)
    local pos_x, pos_y = map:get_hero():get_position()
    local curr_x, curr_y = minimap:convert_coords(pos_x, pos_y)
    minimap.cursor:draw(minimap.surface, curr_x, curr_y)
  end

  -- Convert on map real coordinates to minimap coordinates
  function minimap:convert_coords(on_map_x, on_map_y)

    -- Get map width
    local map_width, map_height = game:get_map():get_size()

    -- Convert coordinates
    local conv_x = on_map_x * minimap_width / map_width + cursor_x_offset
    local conv_y = on_map_y * minimap_height / map_height + cursor_y_offset

    return conv_x, conv_y
  end

  function minimap:set_visible(visible)
    minimap.visible = visible
  end

  return minimap

end

return minimap_builder