-- This script provides configuration information about text and languages.
--
-- Usage:
-- local language_manager = require("scripts/language_manager")

local language_manager = {}

local default_language = "fr"

-- Returns the id of the default language.
function language_manager:get_default_language()
  return default_language
end

-- Returns the font and font size to be used for dialogs
-- depending on the specified language (the current one by default).
function language_manager:get_dialog_font(language)

  -- No font differences between languages (for now).
  return "kubasta", 10
end

-- Returns the font and font size to be used to display text in menus
-- depending on the specified language (the current one by default).
function language_manager:get_menu_font(language)

  -- No font differences between languages (for now).
  return "kubasta", 10
end

-- Returns the fond and font size to be used to display labels like
-- location labels when entering a region
function language_manager:get_label_font(language)

  -- No font differences between languages (for now).
  return "kubasta", 10
end

-- Returns the fond and font size to be used to display contributor names
function language_manager:get_credits_font(language)

  return "alttp_blue", 10
end

return language_manager
