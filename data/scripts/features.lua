-- Sets up all non built-in gameplay features specific to this quest.

-- Usage: require("scripts/features")

-- Features can be enabled to disabled independently by commenting
-- or uncommenting lines below.

require("scripts/debug")
require("scripts/equipment")
require("scripts/dungeons")
require("scripts/menus/game_over")
require("scripts/menus/dialog_box")
require("scripts/menus/pause")
require("scripts/hud/hud")
require("scripts/maps/light_manager.lua")
require("scripts/meta/map")
require("scripts/meta/camera")
require("scripts/meta/chest")
require("scripts/meta/destructible")
require("scripts/meta/dynamic_tile")
require("scripts/meta/npc")
require("scripts/meta/pickable")
require("scripts/meta/sensor")
require("scripts/meta/separator")
require("scripts/meta/shop_treasure")
require("scripts/meta/stairs")
require("scripts/meta/switch")

return true
