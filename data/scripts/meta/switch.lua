-- Provides additional features to the switch type for this quest.

require("scripts/multi_events")

local switch_meta = sol.main.get_metatable("switch")

switch_meta:register_event("on_activated", function(switch)

  sol.audio.play_sound("switch")

  -- Direct open doors on activated
  local open_doors_prop = switch:get_property("open_doors")
  if open_doors_prop ~= nil then
    switch:get_map():open_doors(open_doors_prop)
  end

  -- Direct close doors on activated
  local close_doors_prop = switch:get_property("close_doors")
  if close_doors_prop ~= nil then
    switch:get_map():close_doors(close_doors_prop)
  end

  -- Get name for specific switches
  local name = switch:get_name()
  if name == nil then
    return
  end  

  -- Switches named "lever_switch*" are re-activable and have two
  -- alternative visuals.
  if name:match("^lever_switch") then
    local sprite = switch:get_sprite()
    local direction = sprite:get_direction()
    sprite:set_direction(1 - direction)  -- Direction may be 0 or 1.

    -- Allow to activate it again.
    sol.timer.start(switch, 1000, function()
      switch:set_activated(false)
    end)
  end

end)

switch_meta:register_event("on_created", function(switch)

  local press_on_start_variable = switch:get_property("press_on_start_variable")
  if press_on_start_variable ~= nil then
    local game = switch:get_map():get_game()
    if game:get_value(press_on_start_variable) == true then
      switch:set_activated(true)
    end
  end
end)

return true
