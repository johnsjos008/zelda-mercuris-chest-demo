require("scripts/multi_events")

local chest_meta = sol.main.get_metatable("chest")

chest_meta:register_event("on_opened", function(chest, treasure_item, treasure_variant, treasure_savegame_variable)
  local map = chest:get_map()
  local hero = map:get_hero()
  local game = map:get_game()
  if treasure_item:get_name() == "bomb" then
    if not game:has_item("bomb_bag") then
      hero:start_treasure("rupee", 2)
      return
    end
  end
  hero:start_treasure(treasure_item:get_name(), treasure_variant)
end)

return true