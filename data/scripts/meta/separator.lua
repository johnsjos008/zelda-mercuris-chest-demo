require("scripts/multi_events")

local separator_meta = sol.main.get_metatable("separator")

local enemy_starting_positions = {}

separator_meta:register_event("on_activating", function(separator, direction4)

  local map = separator:get_map()
  local hero = map:get_hero()

  -- disable enemies in the current room
  for entity in map:get_entities_in_region(map:get_camera()) do
    local is_boss = entity:get_property("is_boss") == "true"
    if entity:get_type() == "enemy" and not is_boss then
      entity:set_enabled(false)
      -- reset enemy position to its starting location
      local address = string.format("%p", entity)
      local pos = enemy_starting_positions[address]
      if pos ~= nil then
        entity:set_position(pos[1], pos[2])
      end
    end
  end

  -- clear enemy positions from previous room
  local count = #enemy_starting_positions
  for j = 1, count do enemy_starting_positions[j] = nil end

  -- compute next room position
  local target_x, target_y = hero:get_position()
  if direction4 == 0 then
    target_x = target_x + 16
  elseif direction4 == 1 then
    target_y = target_y - 16
  elseif direction4 == 2 then
    target_x = target_x - 16
  elseif direction4 == 3 then
    target_y = target_y + 16
  end

  -- enable enemies in the next room
  for entity in map:get_entities_in_region(target_x, target_y) do
    local is_boss = entity:get_property("is_boss") == "true"
    if entity:get_type() == "enemy" and not is_boss then
      entity:set_enabled(true)
      local address = string.format("%p", entity)

      -- save enemy starting location when leaving the room
      local pos_x, pos_y = entity:get_position()
      enemy_starting_positions[address] = { pos_x, pos_y }
    end
  end
end)

return true
