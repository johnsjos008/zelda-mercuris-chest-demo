require("scripts/multi_events")

local dungeons = {}

-- Weak wall initialize function
-- {dungeon} name of the dungeon following naming convention: "dungeon_{number}"
-- {number} number of the weak wall to initialize
-- Entity names used:
-- - "weak_wall_{number}" is the weak wall entity itself (the crack in the wall)
-- - "weak_wall_{number}_solid" is the dynamic tile that hides the hole in the wall
-- - "weak_wall_{number}_top" is the dynamic tile that shows a crack in the top of the wall
function dungeons:init_weak_wall(dungeon, number)

  local game = sol.main.game
  local map = game:get_map()

  -- Entities
  local wall = map:get_entity("weak_wall_" .. number)
  local solid = map:get_entity("weak_wall_" .. number .. "_solid")
  local top = map:get_entity("weak_wall_" .. number .. "_top")

  -- Exploded event
  wall.on_opened = function()
    sol.audio.play_sound("secret")
    solid:set_enabled(false)
    top:set_enabled(true)
  end

  if game:get_value(dungeon .. "_weak_wall_" .. number) == true then
    -- Weak wall is already destroyed
    solid:set_enabled(false)
    top:set_enabled(true)
  else
    -- Weak wall is intact
    solid:set_enabled(true)
    top:set_enabled(false)
  end
end

function dungeons:open_door_with_cutscene(options)

  local map = sol.main.game:get_map()
  local hero = map:get_hero()
  local door = map:get_entity(options.door)
  local camera = map:get_camera()

  hero:freeze()

  sol.timer.start(500, function()
    camera:start_manual()
    
    local target_mov = sol.movement.create("target")
    target_mov:set_target(options.camera_target[1], options.camera_target[2])
    target_mov:set_speed(500)
    target_mov:set_ignore_obstacles(true)
    target_mov:start(camera, function()
      sol.timer.start(1000, function()
        door:open()
        sol.audio.play_sound("secret")
        sol.timer.start(1000, function()
          local return_mov = sol.movement.create("target")
          return_mov:set_target(options.camera_return[1], options.camera_return[2])
          return_mov:set_speed(500)
          return_mov:set_ignore_obstacles(true)
          return_mov:start(camera, function()
            hero:unfreeze()
            camera:start_tracking(hero)
          end)
        end)
      end)
    end)
  end)
end

function dungeons:reveal_chest_with_cutscene(options)

  local map = sol.main.game:get_map()
  local hero = map:get_hero()
  local chest = map:get_entity(options.chest)
  local camera = map:get_camera()

  hero:freeze()

  sol.timer.start(500, function()
    camera:start_manual()
    
    local target_mov = sol.movement.create("target")
    target_mov:set_target(options.camera_target[1], options.camera_target[2])
    target_mov:set_speed(500)
    target_mov:set_ignore_obstacles(true)
    target_mov:start(camera, function()
      sol.timer.start(1000, function()
        chest:set_enabled(true)
        sol.audio.play_sound("chest_appears")
        sol.timer.start(1000, function()
          local return_mov = sol.movement.create("target")
          return_mov:set_target(options.camera_return[1], options.camera_return[2])
          return_mov:set_speed(500)
          return_mov:set_ignore_obstacles(true)
          return_mov:start(camera, function()
            hero:unfreeze()
            camera:start_tracking(hero)
          end)
        end)
      end)
    end)
  end)
end

function dungeons:init_room_with_enemy_group(enemy_group_prefix, on_success)
  local game = sol.main.game
  local map = game:get_map()

  local group_sensors_active = true

  local function enemy_died(enemy)
    if map:get_entities_count(enemy_group_prefix .. "_enemy") == 0 then
      sol.audio.play_sound("secret")
      group_sensors_active = false
      if on_success ~= nil then
        on_success()
      end
    end
  end

  local function sensor_activated(sensor)
    if group_sensors_active == true then
      map:close_doors(enemy_group_prefix .. "_door")
      group_sensors_active = false
    end
  end

  for enemy in map:get_entities(enemy_group_prefix) do
    enemy.on_dead = enemy_died
  end

  for sensor in map:get_entities(enemy_group_prefix .. "_sensor") do
    sensor.on_activated = sensor_activated
  end

  map:set_doors_open(enemy_group_prefix .. "_door")
end

function dungeons:init_saved_chest_visibility(chest_name, save_variable)

  local game = sol.main.game
  local map = game:get_map()

  if sol.main.game:get_value(save_variable) ~= true then
    map:get_entity(chest_name):set_enabled(false)
  end
end

function dungeons:init_all_lit_torch_puzzle(torch_prefix, number, on_resolved, on_unresolved)
  
  local game = sol.main.game
  local map = game:get_map()
  local torch_lit_count = 0

  for torch in map:get_entities(torch_prefix) do
    torch:get_sprite().on_animation_changed = function(sprite, animation)
      if animation == "lit" then
        torch_lit_count = torch_lit_count + 1
      else
        torch_lit_count = torch_lit_count - 1
      end

      if torch_lit_count == number then
        on_resolved()
      else
        if on_unresolved ~= nil then
          on_unresolved()
        end
      end
    end
  end
end

function dungeons:disable_all_enemies_except_for_current_room(map)

  -- Disable all enemies in the map
  for enemy in map:get_entities_by_type("enemy") do
    local is_boss = enemy:get_property("is_boss") == "true"
    if not is_boss then
      enemy:set_enabled(false)
    end
  end

  -- Enable enemies in the same room
  local hero = map:get_hero()
  for entity in map:get_entities_in_region(hero) do
    if entity:get_type() == "enemy" then
      entity:set_enabled(true)
    end
  end
end

return dungeons