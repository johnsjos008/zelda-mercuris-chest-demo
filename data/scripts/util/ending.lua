local ending = {}

function ending:start(next_map, on_no_ending)

  local game = sol.main.game
  local map = game:get_map()
  local hero = map:get_hero()
  local camera = map:get_camera()

  if game:get_value("demo_ending") then

    local camera_start_spot = map:get_entity("ending_camera_spot")
    local camera_end_spot = map:get_entity("ending_camera_spot_end")

    hero:set_visible(false)
    
    game:set_hud_enabled(true) -- Restore the HUD after black map
    game:set_demo_ending_frame_only_visible() -- Hide the entire HUD but the ending frame

    -- Init camera position
    camera:start_manual()
    camera:set_position(camera_start_spot:get_position())
    
    -- Create the camera movement
    local mov = sol.movement.create("target")
    mov:set_speed(32)
    mov:set_target(camera_end_spot)
    mov:set_ignore_obstacles(true)
    mov:start(camera)
    mov.on_finished = function()
      hero:teleport(next_map)
    end
  elseif on_no_ending ~= nil then
    on_no_ending()
  end
end

function ending:start_static(waiting_time, on_finished, on_no_ending)

  local game = sol.main.game
  local map = game:get_map()
  local hero = map:get_hero()
  local camera = map:get_camera()

  if game:get_value("demo_ending") then

    local camera_start_spot = map:get_entity("ending_camera_spot")

    hero:set_visible(false)
    game:set_hud_enabled(true) -- Restore the HUD after black map
    game:set_demo_ending_frame_only_visible() -- Hide the entire HUD but the ending frame

    -- Init camera position
    camera:start_manual()
    camera:set_position(camera_start_spot:get_position())
    
    sol.timer.start(map, waiting_time, function()
      on_finished()
    end)
  elseif on_no_ending ~= nil then
    on_no_ending()
  end
end

-- Should be called on map transition finished
function ending:init_hero()

  local game = sol.main.game
  if game:get_value("demo_ending") then
    local map = game:get_map()
    local hero = map:get_hero()
    game:set_pause_allowed(false)
    hero:freeze()
  end
end

return ending