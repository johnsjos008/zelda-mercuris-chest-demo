local audio = {}

function audio:init_music_volume()

  local game = sol.main.game
  game:set_value("settings_music_volume", sol.audio.get_music_volume())
end

function audio:music_fade_out(duration, on_finished)
  
  local game = sol.main.game
  local saved_music_volume = sol.audio.get_music_volume()
  if game ~= nil then
    saved_music_volume = game:get_value("settings_music_volume")
  end
  local current_volume = saved_music_volume

  sol.timer.start(duration / 100, function()

    current_volume = current_volume - (saved_music_volume / 100)
    sol.audio.set_music_volume(math.floor(current_volume))

    if current_volume <= 0 then
      current_volume = 0
      if on_finished ~= nil then
        on_finished()
      end
    end

    return current_volume > 0
  end)
end

function audio:music_fade_in(duration, on_finished)
  
  local game = sol.main.game
  local saved_music_volume = game:get_value("settings_music_volume")
  local current_volume = saved_music_volume

  sol.timer.start(duration / 100, function()

    current_volume = current_volume + (saved_music_volume / 100)
    sol.audio.set_music_volume(math.floor(current_volume))

    if current_volume >= saved_music_volume then
      current_volume = saved_music_volume
      if on_finished ~= nil then
        on_finished()
      end
    end

    return saved_music_volume < saved_music_volume
  end)
end

-- Lower down the music volume according to a factor
-- factor from 0 to 100
function audio:music_quiet(factor)

  local saved_music_volume = game:get_value("settings_music_volume")
  sol.audio.set_music_volume(math.floor(saved_music_volume * factor / 100))
end

function audio:reset_music_volume()

  local game = sol.main.game
  local saved_volume = 100
  if game ~= nil then
    saved_volume = game:get_value("settings_music_volume")
  end
  sol.audio.set_music_volume(saved_volume)
end

return audio