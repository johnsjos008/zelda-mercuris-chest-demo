require("scripts/multi_events")
local npc = require("scripts/maps/npc")
local cutscene = require('scripts/maps/cutscene')

local map = ...
local game = map:get_game()
local hero = map:get_hero()

map:register_event("on_started", function()
  
  for entity in map:get_entities("torch") do
    entity:get_sprite():set_animation("lit")
  end

  if torina_stone_item == nil and piece_of_heart_item ~= nil then
    -- Torina stone has been bought already
    piece_of_heart_item:set_enabled(true)
  end

  npc:init_talk_with_torin("juco", "torin_forest")
end)

muro:register_event("on_interaction", function()

  if game:get_value("torin_forest_torina_stone_read") == true then
    if game:has_ability("lift", 2) == true then
      game:start_dialog("torin_forest.muro.after_pendant")
    elseif game:get_value("torin_forest_muro_pendant") == true then
      hero:freeze()
      cutscene.builder(game, map, hero)
        .dialog("torin_forest.muro.pendant_back")
        .wait(250)
        .exec(function()
          hero:unfreeze()
          hero:start_treasure("glove", 1)
        end)
        .start()
    else
      game:start_dialog("torin_forest.muro.normal")
    end
  else
    game:start_dialog("torin_forest.muro.untranslated")
  end
end)