require("scripts/multi_events")
local util = require("scripts/util/dungeons")

local map = ...
local game = map:get_game()

function map:on_started()

  util:init_weak_wall("torin_forest", 1)
  if game:get_value("torin_forest_torina_stone_read") == true then
    torina_stone:get_sprite():set_animation("repaired")
  end
end

torina_stone:register_event("on_interaction", function()

  if game:get_value("torin_forest_torina_stone_read") ~= true then
    if game:get_value("item_status_piece_of_torina_stone_amount") == 5 then
      sol.audio.play_sound("switch")
      torina_stone:get_sprite():set_animation("repaired")
      hero:freeze()
      sol.timer.start(map, 1000, function()
        game:start_dialog("torin_forest.torina_stone.repaired")
        game:set_value("torin_forest_torina_stone_read", true)
        hero:unfreeze()
      end)
    else
      game:start_dialog("torin_forest.torina_stone.broken")
    end
  else
    game:start_dialog("torin_forest.torina_stone.repaired")
  end
end)