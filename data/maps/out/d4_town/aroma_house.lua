local map = ...
local game = map:get_game()
local hero = map:get_hero()

local cutscene = require('scripts/maps/cutscene')

aroma:register_event("on_interaction", function()
  
  if game:get_value("town_aroma_second_talk") == true then
    -- Dialog when Link has already talked to her after Mercuris' Chest
    game:start_dialog("town.aroma.final")

  elseif game:get_value("crater_temple_tannek_after") == true then
    -- Dialog when Link has missed the Mercuris' Chest
    hero:freeze()
    cutscene.builder(game, map, hero)
      .dialog("town.aroma.second.1")
      .wait(250)
      .dialog("town.aroma.second.2")
      .wait(250)
      .dialog("town.aroma.second.3")
      .exec(function()
        hero:unfreeze()
        game:set_value("town_aroma_second_talk", true)
      end)
      .start()

  else
    -- First dialog
    if game:get_value("town_aroma_first_talk") ~= true then
      hero:freeze()
      cutscene.builder(game, map, hero)
        .dialog("town.aroma.first.1")
        .wait(250)
        .dialog("town.aroma.first.2")
        .exec(function()
          hero:unfreeze()
        end)
        .start()
    else
      game:start_dialog("town.aroma.first.2")
    end
    
  end
end)
