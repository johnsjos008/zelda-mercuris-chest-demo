local map = ...
local game = map:get_game()
local hero = map:get_hero()

local cutscene = require('scripts/maps/cutscene')

function map:on_started()
  smith_table:set_drawn_in_y_order(true)  -- Make sure the smith does not appear above is table.
  local x, y, width, height = smith_table:get_bounding_box()
  local origin_x, origin_y = width / 2, height - 3
  smith_table:set_origin(width / 2, height - 3)
  smith_table:set_position(x + origin_x, y + origin_y)
end

function smith_2:on_interaction()

  map:talk_to_smith()
end

function map:talk_to_smith()

  if game:get_value("town_smith_first_talk") ~= true then
    game:set_value("town_smith_first_talk", true)
    if game:has_item("iron_ore") then
      map:has_iron_ore_first_time()
    else
      map:first_time_talk()
    end
  elseif game:has_item("iron_ore") then
    if game:has_ability("sword", 1) then
      map:after_buying_sword()
    else
      map:come_back_with_iron_ore()
    end
  elseif game:has_ability("shield") then
    map:after_buying_shield()
  elseif game:get_money() >= 20 then
    map:come_back_with_money()
  else
    map:come_back_with_money()
  end
end

function map:shield_trade(answer)
  
  if answer == 1 then
    if game:get_money() < 20 then
      sol.audio.play_sound("wrong")
      game:start_dialog("town.smith.not_enough_money")
    else
      game:remove_money(20)
      hero:start_treasure("shield", 1)
    end
  end
end

function map:sword_trade(answer, callback)
  
  if answer == 1 then
    if game:get_money() < 30 then
      sol.audio.play_sound("wrong")
      game:start_dialog("town.smith.not_enough_money")
    else
      game:remove_money(30)
      game:start_dialog("town.smith.sword_craft.2", function()
        hero:start_treasure("sword", 1, "town_sword_1", callback)
        hero:start_victory()
      end)
    end
  end
end

function map:first_time_talk()

  hero:freeze()
  cutscene.builder(game, map, hero)
    .dialog("town.smith.first.1")
    .wait(500)
    .dialog("town.smith.first.2")
    .wait(500)
    .dialog("town.smith.first.3")
    .wait(500)
    .dialog("town.smith.first.4", function(answer)
      map:shield_trade(answer)
      hero:unfreeze()
    end)
    .start()
end

function map:come_back_with_money()

  -- Selling the shield
  game:start_dialog("town.smith.trade_shield", function(answer)
    map:shield_trade(answer)
  end)
end

function map:after_buying_shield()

  -- Good luck
  game:start_dialog("town.smith.good_luck_for_mine")
end

function map:come_back_with_iron_ore()

  -- Selling the sword
  game:start_dialog("town.smith.sword_craft.1", function(answer)
    map:sword_trade(answer, function()
      game:start_dialog("town.smith.sword_craft.3")
    end)
  end)
end

function map:has_iron_ore_first_time()

  -- Selling the sword directly
  game:start_dialog("town.smith.quick_sword_craft", function(answer)
    map:sword_trade(answer, function()
      game:start_dialog("town.smith.sword_craft.3")
    end)
  end)
end

function map:after_buying_sword()

  if game:has_ability("shield", 1) then
    game:start_dialog("town.smith.future_sword")
  else
    game:start_dialog("town.smith.shield_after_sword", function(answer)
      map:shield_trade(answer)
    end)
  end
end