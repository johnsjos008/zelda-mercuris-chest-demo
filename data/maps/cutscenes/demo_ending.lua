local ending = require("scripts/util/ending")

local map = ...
local game = map:get_game()
local hero = map:get_hero()

function map:on_started()
  thanks_text:set_enabled(false)
  if game:get_value("demo_ending") then
    game:set_hud_enabled(false)
    local shield = hero:get_sprite("shield")
    if shield ~= nil then
      shield:set_opacity(0)
    end

    sol.timer.start(map, 2000, function()
      sol.audio.play_music("fanfare")
      thanks_text:set_enabled(true)
      hero:get_sprite():set_animation("brandish")
      hero:get_sprite():set_direction(3)
    end)

    sol.timer.start(map, 5000, function()
      thanks_text:set_enabled(false)
      hero:start_victory(function()
        hero:get_sprite("sword"):set_opacity(0)
        hero:get_sprite():set_animation("walking")
        hero:get_sprite():set_direction(1)
        local mov = sol.movement.create("straight")
        mov:set_speed(48)
        mov:set_angle(math.pi / 2)
        mov:set_ignore_obstacles(true)
        mov:set_max_distance(300)
        mov:start(hero)
      end)
    end)

    sol.timer.start(map, 10000, function()
      sol.audio.stop_music()
    end)

    sol.timer.start(map, 12000, function()
      sol.main.reset()
    end)
    
    hero:set_visible(true)
  else
    -- Start the ending with a visit of future maps
    sol.audio.play_music("dark_world")

    hero:set_visible(false)
    hero:freeze()
    game:set_hud_enabled(false)

    game:set_value("demo_ending", true)

    sol.timer.start(map, 500, function()
      hero:teleport("out/c3")
    end)
  end
end

function map:on_opening_transition_finished()

  ending:init_hero()
end
