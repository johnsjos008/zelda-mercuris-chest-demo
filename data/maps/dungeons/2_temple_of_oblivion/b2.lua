require("scripts/multi_events")
local util = require("scripts/util/dungeons")

local map = ...
local hero = map:get_hero()
local game = map:get_game()

map:register_event("on_started", function(map, destination)

  map:set_doors_open("blocker_miniboss")
  map:set_doors_open("blocker_boss")
  map:set_doors_open("item_miniboss_door")
  map:set_doors_open("treasure_room_door")

  map:init_puzzles();

  map:check_rotate_pillar_cutscene(destination)
  util:disable_all_enemies_except_for_current_room(map)
end)

map:register_event("on_opening_transition_finished", function(map, destination)

  map:after_check_rotate_pillar_cutscene(destination)
end)

function map:init_puzzles()

  map:init_treasure_room_torch_puzzle()

  rotating_pillar:init({
    floor = "b2",
    savegame_variable = "dungeon_2_b2_rotating_pillar",
    default_side = "top",
  })

  map:init_zombo_miniboss()
  map:init_invisible_tiles_room()
  map:init_boss()
end

function map:check_rotate_pillar_cutscene(destination)

  local dst_name = destination:get_name()
  if dst_name == "rp_cutscene_clockwise" or dst_name == "rp_cutscene_counter_clockwise" then

    hero:set_visible(false)

    sol.timer.start(map, 1000, function()
      if dst_name == "rp_cutscene_clockwise" then
        rotating_pillar:rotate_clockwise(function()
          sol.timer.start(map, 1000, function()
            hero:teleport("dungeons/2_temple_of_oblivion/b1", dst_name)
          end)
        end)
      else
        rotating_pillar:rotate_counter_clockwise(function()
          sol.timer.start(map, 1000, function()
            hero:teleport("dungeons/2_temple_of_oblivion/b1", dst_name)
          end)
        end)
      end
    end)
  end
end

function map:after_check_rotate_pillar_cutscene(destination)

  local dst_name = destination:get_name()
  if dst_name == "rp_cutscene_clockwise" or dst_name == "rp_cutscene_counter_clockwise" then

    hero:freeze()
    game:set_pause_allowed(false)
  end
end

------------------------------------
-- Item miniboss
------------------------------------
-- Miniboss save state
local item_miniboss_sensor_activated = game:get_value("dungeon_2_item_miniboss")

item_miniboss_door_sensor:register_event("on_activated", function()

  if item_miniboss_sensor_activated ~= true then
    map:close_doors("item_miniboss_door")
    sol.audio.play_music("shield/oot_mini_boss")
    item_miniboss_sensor_activated = true
  end
end)

if item_miniboss ~= nil then
  item_miniboss:register_event("on_dead", function()
    sol.audio.play_sound("secret")
    sol.audio.play_music("dark_world_dungeon")
    map:open_doors("item_miniboss_door")
    item_miniboss_sensor_activated = true
  end)
end

------------------------------------
-- Treasure room
------------------------------------

treasure_room_sensor:register_event("on_activated", function()

  map:close_doors("treasure_room_door")
end)

function map:init_treasure_room_torch_puzzle()

  util:init_all_lit_torch_puzzle("torch_treasure_room", 2, function()
    -- on resolved
    map:open_doors("treasure_room_door")
    sol.audio.play_sound("secret")
  end)
end

------------------------------------
-- Zombo miniboss
------------------------------------

blocker_miniboss_sensor:register_event("on_activated", function()

  if game:get_value("dungeon_2_miniboss") ~= true then
    map:close_doors("blocker_miniboss")
    blocker_miniboss_sensor:set_enabled(false)
    zombo:set_enabled(true)
    zombo:set_visible(false)
  end
end)

function map:init_zombo_miniboss()

  -- Enable/Disable miniboss entrance sensor from save
  blocker_miniboss_sensor:set_enabled(not game:get_value("dungeon_2_miniboss"))

  local first_reveal = false

  if game:get_value("dungeon_2_miniboss") ~= true then
    util:init_all_lit_torch_puzzle("torch_zombo", 2, function()
      -- on lit
      if first_reveal == false then
        sol.audio.play_music("shield/oot_mini_boss")
      end
      if zombo ~= nil then
        zombo:set_visible(true)
        sol.audio.play_sound("boss_hurt")
      end
    end, function()
      -- on unlit
      if zombo ~= nil then
        zombo:set_visible(false)
      end
    end)

    zombo.on_dead = function()
      sol.audio.play_sound("secret")
      sol.audio.play_music("dark_world_dungeon")
      map:open_doors("blocker_miniboss")
      game:set_value("dungeon_2_miniboss", true)
    end
  end
end

------------------------------------
-- Invisible tiles room
------------------------------------

function map:init_invisible_tiles_room()

  for floor in map:get_entities("glass_floor") do
    floor:set_visible(false)
  end

  util:init_all_lit_torch_puzzle("torch_invisible_bridge", 1, function()
    -- on lit
    for floor in map:get_entities("glass_floor") do
      floor:set_visible(true)
    end
  end, function()
    -- on unlit
    for floor in map:get_entities("glass_floor") do
      floor:set_visible(false)
    end
  end)
end

------------------------------------
-- Boss
------------------------------------

blocker_boss_sensor:register_event("on_activated", function()

  map:close_doors("blocker_boss")
  sol.audio.play_music("boss")
  shamablin:start()
end)

function map:init_boss()

  -- Enable/Disable boss entrance sensor from save
  blocker_boss_sensor:set_enabled(not game:get_value("dungeon_2_boss"))  
end