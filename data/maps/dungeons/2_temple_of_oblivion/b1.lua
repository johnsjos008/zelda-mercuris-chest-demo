require("scripts/multi_events")
local util = require("scripts/util/dungeons")

local map = ...
local hero = map:get_hero()
local game = map:get_game()

local flying_tile_manager = require("scripts/maps/flying_tile_manager")

----------------------------------------------
-- Init
----------------------------------------------

-- Start
map:register_event("on_started", function (map, destination)

  map:init_puzzles()
  map:check_from_rotating_pillar_cutscene(destination)
  util:disable_all_enemies_except_for_current_room(map)
end)

map:register_event("on_opening_transition_finished", function(map, destination)

  map:after_check_from_rotating_pillar_cutscene(destination)
end)

-- All puzzles of this floor
function map:init_puzzles()

  -- Weak wall near hint stone
  util:init_weak_wall("dungeon_2", 2)

  map:init_magic_torches_puzzle()
  map:init_pillar_torches()
  map:init_evil_tiles()
end

----------------------------------------------
-- Flying evil tiles
----------------------------------------------
function map:init_evil_tiles()
  
  if game:get_value("dungeon_2_evil_tiles") ~= true then
    flying_tile_manager:create_flying_tiles(map, "evil_tile", function()
      -- on finished
      sol.audio.play_sound("secret")
      game:set_value("dungeon_2_evil_tiles", true)
    end)
  else
    -- destroy tiles and open doors
    for tile in map:get_entities("evil_tile_enemy") do
      tile:remove()
    end
    map:set_doors_open("evil_tile_door")
  end
end

----------------------------------------------
-- Rotating pillar
----------------------------------------------

function map:init_pillar_torches()

  if game:get_value("dungeon_2_1f_rotating_pillar") ~= "left" then
    flame_counter_clockwise:set_enabled(true)
    flame_clockwise:set_enabled(false)
    torch_pillar_counter_clockwise:set_enabled(false)
    torch_pillar_clockwise:set_enabled(true)
  else
    flame_counter_clockwise:set_enabled(false)
    flame_clockwise:set_enabled(true)
    torch_pillar_counter_clockwise:set_enabled(true)
    torch_pillar_clockwise:set_enabled(false)
  end
end

local pillar_torch_already_lit = false

torch_pillar_clockwise:register_event("on_interaction", function()
  game:start_dialog("_torch_need_lamp")
end)

torch_pillar_clockwise:register_event("on_collision_fire", function()

  if not pillar_torch_already_lit then
    if game:get_value("dungeon_2_1f_rotating_pillar") ~= "left" then
      pillar_torch_already_lit = true
      sol.audio.play_sound("secret")
      flame_clockwise:set_enabled(true)
      flame_counter_clockwise:set_enabled(false)
      map:start_pillar_cutscene("clockwise")
    end
  end
end)

torch_pillar_counter_clockwise:register_event("on_interaction", function()
  game:start_dialog("_torch_need_lamp")
end)

torch_pillar_counter_clockwise:register_event("on_collision_fire", function()

  if not pillar_torch_already_lit then
    if game:get_value("dungeon_2_1f_rotating_pillar") == "left" then
      pillar_torch_already_lit = true
      sol.audio.play_sound("secret")
      flame_clockwise:set_enabled(false)
      flame_counter_clockwise:set_enabled(true)
      map:start_pillar_cutscene("counter_clockwise")
    end
  end
end)

-- Start cutscene
function map:start_pillar_cutscene(direction)

  hero:freeze()
  game:set_pause_allowed(false)
  sol.timer.start(map, 1000, function()
    hero:teleport("dungeons/2_temple_of_oblivion/1f", "rp_cutscene_" .. direction)
  end)
end

-- Return from B2 cutscene
function map:check_from_rotating_pillar_cutscene(destination)

  local dst_name = destination:get_name()
  if dst_name == "rp_cutscene_clockwise" or dst_name == "rp_cutscene_counter_clockwise" then
    hero:set_visible(true)
  end
end

function map:after_check_from_rotating_pillar_cutscene(destination)
  local dst_name = destination:get_name()
  if dst_name == "rp_cutscene_clockwise" or dst_name == "rp_cutscene_counter_clockwise" then
    hero:unfreeze()
    game:set_pause_allowed(true)
  end
end

----------------------------------------------
-- Magic torches
----------------------------------------------

function map:init_magic_torches_puzzle()

  -- All torch states
  local torch_states = {
    torch_magic_1 = false,
    torch_magic_2 = false,
    torch_magic_3 = false,
    torch_magic_4 = false,
  }

  -- Prevent from multiple actions of liting a torch when collision with fire
  local already_lit = {
    torch_magic_1 = false,
    torch_magic_2 = false,
    torch_magic_3 = false,
    torch_magic_4 = false,
  }

  -- Change the state of a torch
  local function toggle_torch(name)
    local torch_sprite = map:get_entity(name):get_sprite()
    if torch_states[name] then
      already_lit[name] = false
      torch_states[name] = false
      torch_sprite:set_animation("unlit")
    else
      torch_states[name] = true
      torch_sprite:set_animation("lit")
    end
  end

  -- Determine which torch are lit or unlit
  local function lit_torch(name)
    if name == "torch_magic_1" then
      toggle_torch("torch_magic_1")
      toggle_torch("torch_magic_2")
      toggle_torch("torch_magic_3")
    elseif name == "torch_magic_2" then
      toggle_torch("torch_magic_1")
      toggle_torch("torch_magic_2")
      toggle_torch("torch_magic_4")
    elseif name == "torch_magic_3" then
      toggle_torch("torch_magic_1")
      toggle_torch("torch_magic_3")
      toggle_torch("torch_magic_4")
    else
      toggle_torch("torch_magic_2")
      toggle_torch("torch_magic_3")
      toggle_torch("torch_magic_4")
    end

    -- If all torches are lit, make chest appear
    if torch_states["torch_magic_1"] == true
      and torch_states["torch_magic_2"] == true
      and torch_states["torch_magic_3"] == true
      and torch_states["torch_magic_4"] == true then
      sol.audio.play_sound("secret")
      magic_torch_chest:set_enabled(true)
    end
  end

  
  for torch in map:get_entities("torch_magic") do

    if game:get_value("dungeon_2_key_3") ~= true then
      
      -- Puzzle has not been resolved yet
      torch.on_collision_fire = function ()
        local torch_name = torch:get_name()
        if not already_lit[torch_name] then
          -- Lit a torch
          already_lit[torch_name] = true
          lit_torch(torch_name)
        end
      end
    else

      -- Resolved already: all torches are lit and chest is present
      magic_torch_chest:set_enabled(true)
      torch:get_sprite():set_animation("lit")
    end
  end
end
