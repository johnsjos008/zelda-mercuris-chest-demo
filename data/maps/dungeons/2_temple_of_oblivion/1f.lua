require("scripts/multi_events")
local util = require("scripts/util/dungeons")

local map = ...
local hero = map:get_hero()
local game = map:get_game()

map:register_event("on_started", function(map, destination)

  map:init_puzzles()
  map:check_rotate_pillar_cutscene(destination)
  util:disable_all_enemies_except_for_current_room(map)
end)

map:register_event("on_opening_transition_finished", function(map, destination)

  map:after_check_rotate_pillar_cutscene(destination)
end)

function map:init_puzzles()

  util:init_weak_wall("dungeon_2", 1)

  rotating_pillar:init({
    floor = "1f",
    savegame_variable = "dungeon_2_1f_rotating_pillar",
    default_side = "right",
  })

  map:init_keese_croissant_puzzle()
  map:init_small_portal_puzzle()
  map:init_torch_main_room()
  map:init_boss_key_torch_puzzle()
end

------------------------------------------
-- Rotating pillar cutscene (from B1)
------------------------------------------

function map:check_rotate_pillar_cutscene(destination)

  local dst_name = destination:get_name()
  if dst_name == "rp_cutscene_clockwise" or dst_name == "rp_cutscene_counter_clockwise" then

    hero:set_visible(false)

    sol.timer.start(map, 1000, function()
      if dst_name == "rp_cutscene_clockwise" then
        rotating_pillar:rotate_clockwise(function()
          sol.timer.start(map, 1000, function()
            hero:teleport("dungeons/2_temple_of_oblivion/b2", dst_name)
          end)
        end)
      else
        rotating_pillar:rotate_counter_clockwise(function()
          sol.timer.start(map, 1000, function()
            hero:teleport("dungeons/2_temple_of_oblivion/b2", dst_name)
          end)
        end)
      end
    end)
  end
end

function map:after_check_rotate_pillar_cutscene(destination)

  local dst_name = destination:get_name()
  if dst_name == "rp_cutscene_clockwise" or dst_name == "rp_cutscene_counter_clockwise" then

    hero:freeze()
    game:set_pause_allowed(false)
  end
end

------------------------------------------
-- Entrance room
------------------------------------------

blocker_1_switch:register_event("on_activated", function()

  util:open_door_with_cutscene({
    door = "blocker_1",
    camera_target = { 1104, 1264 },
    camera_return = { 1104, 1462 },
  })
end)

------------------------------------------
-- East room
------------------------------------------

east_room_switch:register_event("on_activated", function()

  local hero = map:get_hero()
  local hero_x, hero_y = hero:get_position()
  util:reveal_chest_with_cutscene({
    chest = "key_chest_1",
    camera_target = { hero_x - 160, 1384 },
    camera_return = { hero_x - 160, 1264 },
  })
end)

------------------------------------------
-- Croissant room
------------------------------------------

function map:init_keese_croissant_puzzle()

  if game:get_value("dungeon_2_resource_chest_2") ~= true then
    util:init_room_with_enemy_group("keese_group_croissant", function()
      croissant_chest:set_enabled(true)
    end)
  else
    croissant_chest:set_enabled(true)
  end
end

------------------------------------------
-- Small portal
------------------------------------------

function map:init_small_portal_puzzle()
  
  local function explode_portal_element(name)
    local entity = map:get_entity(name)
    local x, y, layer = entity:get_position()
    sol.audio.play_sound("explosion")
    map:create_explosion({
      layer = layer,
      x = x + 8,
      y = y + 13,
    })
    entity:remove()
  end

  if game:get_value("dungeon_2_small_portal") ~= true then
    util:init_all_lit_torch_puzzle("torch_small_portal", 2, function()
      game:set_value("dungeon_2_small_portal", true)
      sol.audio.play_sound("secret")
      hero:freeze()
  
      local index = 3
      sol.timer.start(map, 1000, function()
        sol.timer.start(map, 250, function()
          explode_portal_element("small_portal_" .. index)
          index = index - 1
          if index == 0 then
            hero:unfreeze()
          end
          return index > 0
        end)
      end)
    end)
  else
    for portal_element in map:get_entities("small_portal") do
      portal_element:remove()
    end
  end
end

------------------------------------------
-- Torch main room
------------------------------------------

function map:init_torch_main_room()

  if game:get_value("dungeon_2_main_room_chest") == true then
    main_room_chest:set_enabled(true)
  end

  torch_main_room:get_sprite().on_animation_changed = function(sprite, animation)
    if game:get_value("dungeon_2_main_room_chest") ~= true then

      if animation == "lit" then
        util:reveal_chest_with_cutscene({
          chest = "main_room_chest",
          camera_target = { 880 - 160, 789 - 120 },
          camera_return = { 528, 560 },
        })
        game:set_value("dungeon_2_main_room_chest", true)
      end
    end
  end
end

------------------------------------------
-- Torch boss key
------------------------------------------

function map:init_boss_key_torch_puzzle()

  map:init_hint_medusa()

  local lit_count = 0
  local lit_stack = { 0, 0, 0, 0, 0, 0 }

  local function add_to_lit_stack(torch_number)
    lit_stack[1] = lit_stack[2]
    lit_stack[2] = lit_stack[3]
    lit_stack[3] = lit_stack[4]
    lit_stack[4] = lit_stack[5]
    lit_stack[5] = lit_stack[6]
    lit_stack[6] = torch_number
  end

  local function tables_are_equal(table_1, table_2)
    for i, _ in ipairs(table_1) do
      if table_1[i] ~= table_2[i] then
        return false
      end
    end

    return true
  end

  local function check_for_order(on_success, on_failure)

    if tables_are_equal(lit_stack, { 1, 2, 3, 4, 5, 6 }) then
      on_success()
    elseif tables_are_equal(lit_stack, { 2, 3, 4, 5, 6, 1 }) then
      on_success()
    elseif tables_are_equal(lit_stack, { 3, 4, 5, 6, 1, 2 }) then
      on_success()
    elseif tables_are_equal(lit_stack, { 4, 5, 6, 1, 2, 3 }) then
      on_success()
    elseif tables_are_equal(lit_stack, { 5, 6, 1, 2, 3, 4 }) then
      on_success()
    elseif tables_are_equal(lit_stack, { 6, 1, 2, 3, 4, 5 }) then
      on_success()
    else
      on_failure()
    end
  end

  local function reset_all_torches()
    for torch in map:get_entities("torch_top_room") do
      torch:get_sprite():set_animation("unlit")
    end
  end

  local resolved = false

  if game:get_value("dungeon_2_boss_key") ~= true then
    for torch in map:get_entities("torch_top_room") do
      torch:get_sprite().on_animation_changed = function(sprite, animation)
        if not resolved then
          if animation == "lit" then
            lit_count = lit_count + 1
            local number = torch:get_property("number")
            add_to_lit_stack(tonumber(number))
            if lit_count == 6 then
              check_for_order(function()
                -- on success
                sol.audio.play_sound("secret")
                boss_key_chest:set_enabled(true)
              end, function()
                -- on failure
                sol.audio.play_sound("wrong")
                sol.timer.start(map, 1000, function()
                  lit_count = 0
                  reset_all_torches()
                end)
              end)
            end
          end
        end
      end
    end
  else
    boss_key_chest:set_enabled(true)
  end
end

function map:init_hint_medusa()
  local targets = {}

  local i = 1
  for torch in map:get_entities("torch_top_room") do
    local x, y = torch:get_position()
    targets[i] = { x = x, y = y }
    i = i + 1
  end

  i = 1
  sol.timer.start(hint_medusa, 1000, function()
    if hint_medusa:get_distance(hero) < 500 and hint_medusa:is_in_same_region(hero) then
      sol.audio.play_sound("zora")

      local x, y, layer = hint_medusa:get_position()

      map:create_enemy({
        breed = "alttp/fireball_small",
        layer = layer, 
        x = x + 8,
        y = y + 8,
        width = 8,
        height = 8,
        direction = 0,
        properties = {
          {
            key = "target_x",
            value = tostring(targets[i].x),
          },
          {
            key = "target_y",
            value = tostring(targets[i].y),
          },
        }
      })

      i = i + 1
      if i > 6 then
        i = 1
      end
    end
    return true
  end)
end