require("scripts/multi_events")
local util = require("scripts/util/dungeons")

local map = ...
local game = map:get_game()
local hero = map:get_hero()
local camera = map:get_camera()

--------------------------------------
-- Init
--------------------------------------

function map:on_started(destination)

  map:init_puzzles(destination)
  map:init_boss()
  util:disable_all_enemies_except_for_current_room(map)
end

function map:init_puzzles(destination)

  -- Rooms with enemies to kill
  util:init_room_with_enemy_group("tentacle_group", function()
    map:open_doors("tentacle_group_door")
  end)
  util:init_room_with_enemy_group("cobruss_group", function()
    cobruss_chest:set_enabled(true)
    map:open_doors("cobruss_group_door")
  end)

  -- Init 'must-appear' saved chests
  util:init_saved_chest_visibility("vase_room_chest", "dungeon_1_key_1")
  util:init_saved_chest_visibility("cobruss_chest", "dungeon_1_key_2")

  -- Init 'sensor closable' doors
  tentacle_group_door_stairs:set_open(true)
  if destination == south_room then
    tentacle_door_compass:set_open(true)
    map_door_switch:set_activated(true)
  end
end

--------------------------------------
-- First visit intro
--------------------------------------

-- Visit tigger
--entrance_sensor:register_event("on_activated", function()
--
--  if game:get_value("dungeon_1_first_visit") ~= true then
--    map:first_visit_intro()
--  end
--end)

-- Visit camera movement
function map:first_visit_intro()

  hero:freeze()
  camera:start_manual()
  game:set_hud_enabled(false)

  camera:set_position(464, 512)

  sol.timer.start(1500, function()
    local mov = sol.movement.create("target")
    mov:set_speed(48)
    mov:set_target(464, 752)
    mov:set_ignore_obstacles(true)
    mov:start(camera, function()
      hero:unfreeze()
      camera:start_tracking(hero)
      game:set_hud_enabled(true)
      game:set_value("dungeon_1_first_visit", true)
    end)
  end)
end

--------------------------------------
-- Vase room
--------------------------------------

-- Chest switch
vase_chest_switch:register_event("on_activated", function()

  util:reveal_chest_with_cutscene({
    chest = "vase_room_chest",
    camera_target = { 0, 952 },
    camera_return = { 0, 752 },
  })
end)

-- Door switch
vase_door_switch:register_event("on_activated", function()

  util:open_door_with_cutscene({
    door = "blocker_1",
    camera_target = { 0, 752 },
    camera_return = { 0, 952 },
  })
  close_loud_blocker_1_sensor:set_enabled(false)
end)

----------------------------------------
-- Boss
----------------------------------------

function map:init_boss()
  
  if game:get_value("dungeon_1_boss") == true then
    legendary_chest:remove()
    chest_blocker:remove()
  else
    after_boss_teleporter:remove()
  end
end

-- Trigger
boss_sensor:register_event("on_activated", function()

  if game:get_value("dungeon_1_boss") ~= true then
    sol.audio.stop_music()
    mercuris:set_enabled(true)
    boss_sensor:remove()
  end
end)

-- Start victory on obtaining the heart container
-- left by Mercuris
function map:on_obtained_treasure(item, variant, savegame_variable)

  if item:get_name() == "heart_container" then
    hero:freeze()
    hero:start_victory(function()
      sol.timer.start(map, 500, function()
        sol.audio.play_sound("warp")
        hero:teleport("out/c4_crater/temple", "from_dungeon")
      end)
    end)
  end
end